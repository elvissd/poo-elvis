package Aula52Contato;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Agenda agenda = new Agenda();
        int opcao;
        do {
            opcao = obterOpcaoMenu(input);
            if (opcao == 1) {//Consultar contato
                consultarContato(input, agenda);
            } else if (opcao == 2) {//Adicionar contato
                adicionarcontato(input, agenda);
            } else if (opcao == 3) {//Termina o programa - é opcional, pq só faz algo se for 1 ou 2
                System.exit(0);
            }
        } while (opcao != 3);

    }

    public static void consultarContato(Scanner input, Agenda agenda) {
        String nomeContato = leInformacaoString(input, "Entre com o nome do contato a ser pesquisado: ");
        try {
            if (agenda.consultaContatoPornome(nomeContato) >= 0) {
                System.out.println("Contato existe");
            }
        } catch (ContatoNaoExisteException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void adicionarcontato(Scanner input, Agenda agenda) {
        try {
            System.out.println("Criando um contato, entre com as informações!");
            String nome = leInformacaoString(input, "Entre com o nome do contato");
            String telefone = leInformacaoString(input, "Entre com o telefone do contato");
            String email = leInformacaoString(input, "Entre com o email do contato");

            Contato contato = new Contato();
            contato.setNome(nome);
            contato.setTelefone(telefone);
            contato.setEmail(email);

            System.out.println("Contato a ser criado: ");
            System.out.println(contato);//Só contato ou contato.toString

            agenda.adicionarContato(contato);
        } catch (AgendaCheiaException e) {
            System.out.println(e.getMessage());
            System.out.println("Contatos da agenda:");
            agenda.toString();
        }
    }

    public static String leInformacaoString(Scanner input, String msg) {
        System.out.println(msg);
        String entrada = input.nextLine();
        return entrada;
    }

    public static int obterOpcaoMenu(Scanner input) {
        int opcao = 3;
        boolean entradaValida = false;

        while (!entradaValida) {
            System.out.println("Digite a opção desejada: ");
            System.out.println("1: Consultar contato;");
            System.out.println("2: Adicionar contato;");
            System.out.println("3: Sair.");

            try {
                //opcao = input.nextInt(); ou:
                String entrada = input.nextLine();
                opcao = Integer.parseInt(entrada);
                if (opcao == 1 || opcao == 2 || opcao == 3) {
                    entradaValida = true;
                } else {
                    throw new Exception("Entrada inválida!");
                }
            } catch (Exception e) {
                System.out.println("Entrada inválida, digite novamente!\n");
            }
        }

        return opcao;
    }

}

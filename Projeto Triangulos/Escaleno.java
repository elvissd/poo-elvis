package ProjetoTriangulos;

public class Escaleno extends Triangulo{
    private String cor = "Sem cor";
    
    public Escaleno(double A, double B, double C){
        if (validarTriangulo(A, B, C)) {
            this.setBaseA(A);
            this.setAlturaB(B);
            this.setLadoC(C);
        }
    }
    
    //Outro construtor caso o usuário queira  atribuir uma cor
    public Escaleno(double A, double B, double C, String cor){
        this(A, B, C);
        this.setCor(cor);
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
    
    @Override
    public boolean validarTriangulo(double A, double B, double C) {
        if ((A + B > C && B + C > A && A + C > B) && (B != 0 && C != 0)) {
            return true;
        } else {
            return false;
        }
    }
}
package ProjetoTriangulos;

public class Equilatero extends Triangulo{
    
    public Equilatero(double A, double B, double C){
        if (validarTriangulo(A, B, C)) {
            this.setBaseA(A);
            this.setAlturaB(B);
            this.setLadoC(C);
        }
    }
    
    @Override
    public String toString() {
        return "Triangulo Equilatero{" + "base = " + getBaseA() + ", altura = " + getAlturaB() +
                ", ladoC = " + getLadoC() + ", Área = " + getArea() + '}';
    }

    @Override
    public boolean validarTriangulo(double A, double B, double C) {
        if ((A + B > C && B + C > A && A + C > B) && (B != 0 && C != 0)) {
            return true;
        } else {
            return false;
        }
    }
}
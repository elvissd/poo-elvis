package ProjetoTriangulos;

public class Isosceles extends Triangulo {

    private String nome = "Sem nome";

    public Isosceles(double A, double B, double C) {
        if (validarTriangulo(A, B, C)) {
            this.setBaseA(A);
            this.setAlturaB(B);
            this.setLadoC(C);
        }

    }

    //Esse outro construtor é para caso o usuário queira nomear o Triângulo
    public Isosceles(double A, double B, double C, String nome) {
        this(A, B, C);
        this.setNome(nome);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean validarTriangulo(double A, double B, double C) {
        if ((A + B > C && B + C > A && A + C > B) && (B != 0 && C != 0)) {
            return true;
        } else {
            return false;
        }
    }

}

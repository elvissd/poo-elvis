package ProjetoTriangulos;

import java.util.Scanner;

public class GestaoTriangulos {

    static final int tamanho = 100;//este seré o tamanho do vetor, não poderá ser modificado
    static int contTamanho = 0;//Este será o contador para controlar que o usuário não cadastre mais do que o tamanho total do vetor
    static Triangulo vetor[] = new Triangulo[tamanho];//O tamanho do vetor será o definido na variável final tamanho e não pode ser alterado
    private int numIndice = 0;

    /*
    No método abaixo o usuário vai entrar com os valores do triângulo. Depois irei fazer a validação
    Se os 3 lados formarem um triângulo, entrarei com método de cadastrar o tipo no vetor.*/
    public void InserirTriangulo(Triangulo vet[]) {
        Scanner input = new Scanner(System.in);
        double A, B, C;
        do {
            System.out.print("Entre com o lado A do triângulo (BASE) digite 0 para cancelar: ");
            A = input.nextDouble();
            System.out.print("Entre com o lado B (ALTURA): ");
            B = input.nextDouble();
            System.out.print("Entre com o terceiro lado do triângulo: ");
            C = input.nextDouble();
            System.out.println("\n");
            
            if(A != 0 && B != 0 && C != 0) //Para não cadastrar triângulos como Equilátero com todos os lados iguais a 0
            CadastrarVetor(A, B, C, vet);
            contTamanho++;//A cada triângulo cadastrado, será reduzido um número no contador

        } while (A != 0 && contTamanho < tamanho);

        //O programa parará de coletar os dados de triângulo quando for informado o valor 0 para o primeiro lado e mostrará:
        //Imprime a quantidade total de triângulos cadastrada no vetor abaixo:
        System.out.println("Quantidade total de triângulos: " + qtdTriangulos(GestaoTriangulos.vetor));

        //Imprime a quantidade de triângulos por tipo abaixo:
        qtdPorTipo(GestaoTriangulos.vetor);

        //Imprime a área de cada um dos Equiláteros abaixo:
        areaEquilateros(GestaoTriangulos.vetor);

        //Desenha o triângulo com a maior área:
        System.out.println("DESENHO DO TRIÂNGULO COM MAIOR ÁREA: \n" + desenho(GestaoTriangulos.vetor));
    }

    public void CadastrarVetor(double A, double B, double C, Triangulo vet[]) {
        Scanner input = new Scanner(System.in);
        //Erro: GestaoTriangulos p = new GestaoTriangulos();

        Triangulo tri;

        //int numIndice = 0; //Essa variável irá controlar a posição do vetor em que será cadastrado o triângulo
        if (A == B && A == C && B == C) {
            tri = new Equilatero(A, B, C);
            vet[numIndice] = tri;
            numIndice++;
        } else if (A != B && A != C && B != C) {
            String valor;
            do {
                System.out.print("TRIÂNGULO ESCALENO! Deseja atribuir uma cor? Entre com sim ou nao: ");
                valor = input.next();
            } while (!valor.equalsIgnoreCase("sim") && !valor.equalsIgnoreCase("nao"));

            if (valor.equalsIgnoreCase("sim")) {
                System.out.print("Atribua uma cor: ");
                String cor = input.next();
                tri = new Escaleno(A, B, C, cor);
                vet[numIndice] = tri;
            } else {
                tri = new Escaleno(A, B, C);
                vet[numIndice] = tri;
            }
            numIndice++;
        } else {
            String valor2;
            do {
                System.out.print("TRIÂNGULO ISÓSCELES! Deseja nomeá-lo? Entre com sim ou nao: ");
                valor2 = input.next();
            } while (!valor2.equalsIgnoreCase("sim") && !valor2.equalsIgnoreCase("nao"));

            if (valor2.equalsIgnoreCase("sim")) {
                System.out.print("Entre com um nome para o triângulo: ");
                String nomeTri = input.next();

                tri = new Isosceles(A, B, C, nomeTri);
                vet[numIndice] = tri;
                numIndice++;
            } else {
                tri = new Isosceles(A, B, C);
                vet[numIndice] = tri;
                numIndice++;
            }

        }

    }

    public int qtdTriangulos(Triangulo vet[]) {
        int qtdTri = 0;

        for (int i = 0; i < +vet.length; i++) {
            if (vet[i] != null) {
                qtdTri = qtdTri + 1;
            }
        }
        //Irá retornar a quantidade do vetor menos os objetos nulos, ou seja, a quantidade apenas de triângulos
        return qtdTri;
    }

    public void qtdPorTipo(Triangulo vet[]) {
        int qtdEquilatero = 0;
        int qtdEscaleno = 0;
        int qtdIsosceles = 0;

        for (Triangulo v : vetor) {
            if (v instanceof Equilatero) {
                qtdEquilatero++;
            } else if (v instanceof Escaleno) {
                qtdEscaleno++;
            } else if (v instanceof Isosceles) {
                qtdIsosceles++;
            }
        }
        System.out.println("-------------------------------------------------");
        System.out.println("****QUANTIDADE DE TRIÂNGULOS POR TIPO****");
        System.out.println("QUANTIDADE DE EQUILATEROS: " + qtdEquilatero);
        System.out.println("QUANTIDADE DE ESCALENOS: " + qtdEscaleno);
        System.out.println("QUANTIDADE DE ISÓSCELES: " + qtdIsosceles);
        System.out.println("-------------------------------------------------");
    }

    public void areaEquilateros(Triangulo vet[]) {
        int num = 1;
        System.out.println("*****ÁREA DE CADA UM DOS TRIÂNGULOS EQUILATEROS*****");
        for (Triangulo v : vetor) {
            if (v != null && v instanceof Equilatero) {
                System.out.println("Área do Equilátero número " + num + ": " + v.getArea() + " cm².");
                num++;
            }
        }
        System.out.println("-------------------------------------------------");
    }

    public String desenho(Triangulo[] vet) {
        String desenho = "Sem desenho.";
        Triangulo maior = GestaoTriangulos.vetor[0];
        for (Triangulo vet1 : GestaoTriangulos.vetor) {
            if (vet1 != null) { //Primeiro verifico se a posição não contém null
                if (vet1.getArea() >= maior.getArea()) {//Agora comparo somente onde há um objeto instanciado
                    if (vet1 instanceof Equilatero) {
                        desenho
                                = "         ^\n"
                                + "        / \\\n"
                                + "       /   \\\n"
                                + "      /     \\\n"
                                + "     / área: \\\n"
                                + "    /         \\\n"
                                + "   /  " + vet1.getArea() + "cm²  \\\n"
                                + "  /             \\\n"
                                + " *---------------*\n";
                    } else if (vet1 instanceof Escaleno) {
                        desenho
                                = "+\n"
                                + "|\\\n"
                                + "| \\\n"
                                + "|  \\\n"
                                + "|   \\\n"
                                + "|    \\\n"
                                + "|     \\\n"
                                + "|      \\\n"
                                + "|       \\\n"
                                + "| área:  \\\n"
                                + "| " + vet1.getArea() + "cm²  \\\n"
                                + "+----------+\n"
                                + "Cor deste triâgulo Escaleno: " + ((Escaleno) vet1).getCor();
                    } else {
                        desenho
                                = "+\n"
                                + "|\\\n"
                                + "| \\\n"
                                + "|  \\\n"
                                + "|   \\\n"
                                + "|    \\\n"
                                + "|     \\\n"
                                + "|      \\\n"
                                + "|       \\\n"
                                + "| área:  \\\n"
                                + "| " + vet1.getArea() + "cm²  \\\n"
                                + "+----------+\n"
                                + "Nome deste triâgulo Isósceles: " + ((Isosceles) vet1).getNome();
                    }
                }
            }
        }
        return desenho;
    }

}

package ProjetoTriangulos;

public abstract class Triangulo{
    private double baseA;
    private double alturaB;
    private double ladoC;  

    public double getBaseA() {
        return baseA;
    }

    public void setBaseA(double baseA) {
        this.baseA = baseA;
    }

    public double getAlturaB() {
        return alturaB;
    }

    public void setAlturaB(double alturaB) {
        if(alturaB != 0){
            this.alturaB = alturaB;
        }
    }

    public double getLadoC() {
        return ladoC;
    }

    public void setLadoC(double ladoC) {
        if(ladoC != 0){
            this.ladoC = ladoC;
        }
    }

    public double getArea() {
        double area;
        area = this.getBaseA() * this.getAlturaB() / 2;
        return area;
    }
    
    public abstract boolean validarTriangulo(double A, double B, double C);
    
}
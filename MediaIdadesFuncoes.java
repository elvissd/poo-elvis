﻿/* PRIMEIRA QUESTÃO DE POO (AULA 14/03/2017)
1- Preencher doi vetores com tamanho 50, um com idades de 50 homens e outro com idades de 50 mulheres
2 - Calcular a média das idades dos homens e das mulheres
3 - Imprimir a quantidade de homens com idade superior a média das idades das mulheres e vice versa. */
package poo;

import java.util.Scanner;
 
 public class MediaIdadesFuncoes {
     
     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
         
        int masculino[] = new int [5];
        int feminino[] = new int [5];
        double mediaFem = 0, mediaMasc = 0;
        int qtdMasc = 0, qtdFem = 0;
        
        System.out.println("PREENCHER O VETOR MASCULINO");
        preencherVetor(masculino);
        
        System.out.println("PREENCHER O VETOR FEMININO");
        preencherVetor(feminino);
        
        //CALCULAR A MÉDIA DAS IDADES MASCULINAS e FEMININAS
        mediaMasc = media(masculino, qtdMasc);
        mediaFem = media(feminino, qtdFem);
        
        //QUANTIDADE DE HOMENS COM IDADE SUPERIOR À MÉDIA FEMININA
        for (int i = 0; i < masculino.length; i++) {
            if(masculino[i] > mediaFem){
                qtdMasc++;
            }
        }
        
        //QUANTIDADE DE MULHERES COM IDADE SUPERIOR À MÉDIA MASCULINA
        for (int i = 0; i < feminino.length; i++) {
            if(feminino[i] > mediaMasc){
                qtdFem++;
            }
        }
        
         System.out.println("==========RESULTADO==========");
        System.out.println("Quantidade de homens acima da média das mulheres: " + qtdMasc);
        System.out.println("Quantidade de mulheres acima da média dos homens: " + qtdFem);
     }
     
     public static void preencherVetor(int vet[]){
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < vet.length; i++) {
            System.out.print("Informe uma idade na posição " + (i+1) + ": ");
            vet[i] = input.nextInt();
        }
     }
     
     public static double media(int vetor[], int qtd){
        int soma = 0;
        for (int i = 0; i < vetor.length; i++) {
           soma = soma + vetor[i];
        }
        double media = soma / vetor.length;
        return media; //Converti double to int
    }
 
 
 }
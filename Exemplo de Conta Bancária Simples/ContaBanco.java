package aula05;

public class ContaBanco {
    public int numConta;
    protected String tipo;
    private String dono;
    private double saldo;
    private boolean status;
    
    //Método Construct
    public ContaBanco(){
        this.status = false;
        this.saldo = 0;
    }
    
    //Métodos Getters e Setters para os atributos (variáveis)
    public void setnumConta(int num){
        this.numConta = num;
    }
    
    public int getNumConta(){
        return this.numConta;
    }
    
    public void setTipo(String t){
        this.tipo = t;
    }
    
    public String getTipo(){
        return this.tipo;
    }
    
    public void setDono(String d){
        this.dono = d;
    }
    
    public String getDono(){
        return this.dono;
    }
    
    public void setSaldo(double s){
        this.saldo = s;
    }
    
    public double getSaldo(){
        return this.saldo;
    }
    
    public void setStatus(boolean st){
        this.status = st;
    }
    
    public boolean getStatus(){
        return this.status;
    }
    
    //Métodos específicos (seguem uma lógica)
    public void abrirConta(String t){
        setTipo(t);
        setStatus(true);
        if(t.equalsIgnoreCase("CC")){
            setSaldo(50);
        }
        if(t.equalsIgnoreCase("CP")){
            setSaldo(150);
        }
        
        
    }
    
    public void fecharConta(){
        if(getSaldo() > 0){
            System.out.println("Conta com dinheiro.");
        }else if(getSaldo() < 0){
            System.out.println("Conta em débito.");
        }else{
            setStatus(false);
        }
    }
    
    public void depositarValor(double valorDepositar){
        if(getStatus() == true){
            setSaldo(getSaldo() + valorDepositar);
            //ou this.saldo = this.saldo + valorDepositar, mas com set é melhor
        }else{
            System.out.println("Impossível depositar!");
        }
    }
    
    public void sacarValor(double valorSacar){
        if(getStatus() == true){
            if(valorSacar <= getSaldo()){
                setSaldo(getSaldo() - valorSacar);
            }else
                System.out.println("Valor não pode ser sacado.");
        }else{
            System.out.println("Impossível sacar.");
        }        
    }
    
    public void pagarMensalidade(){
        double mensalidade = 0;
        if(getTipo().equalsIgnoreCase("CC")){
            mensalidade = 12;
        }else if(getTipo().equalsIgnoreCase("CP")){
            mensalidade = 20;
        }
        
        if(getStatus() == true){
            if(getSaldo() >= mensalidade){
                setSaldo(getSaldo() - mensalidade);
            }else{
                System.out.println("Saldo insuficiente.");
            }
        }else{
            System.out.println("Impossível cobrar mensalidade!");
        }
    }
    
    
    
    
}
﻿package aula05;

import java.util.Scanner;

public class Aula05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String tipo = "";
        
        //Exemplo de uso do código:
        System.out.println("*******VAMOS ABRIR UMA CONTA BANCÁRIA!********");
        do{
            System.out.println("Entre com um tipo de conta (Digite CC para Corrente" + " ou CP para Poupança)");
            tipo = input.next();
        }while(!tipo.equalsIgnoreCase("CP") && !tipo.equalsIgnoreCase("CC"));
        //Enquanto for diferente da entrada válida, vai pedindo o tipo de conta.
        
        ContaBanco conta1 = new ContaBanco();
        conta1.abrirConta(tipo);
        System.out.print("Informe o nome do titular da conta: ");
        input.nextLine(); //Acrescentei um nextLine para esvaziar o buffer do teclado. \n
        //Antes dava erro eo ler um nome completo.
        conta1.setDono(input.nextLine());
        
        System.out.println("Titular: " + conta1.getDono());
        System.out.println("A conta está aberta? " + conta1.getStatus());
        System.out.println("Tipo da conta: " + conta1.getTipo());
        System.out.println("Saldo: " + conta1.getSaldo());
        
        //Simulando o depósito de R$1000 na conta.
        System.out.print("Entre com o valor a ser depositado: ");
        conta1.depositarValor(input.nextDouble());
        System.out.println("Novo saldo: " + conta1.getSaldo());
        
    }
    
}
/*Implemente uma função recursiva que calcula o somatório
dos n primeiros números inteiros.
Ex.: N = 5
5 + 4 + 3 + 2 + 1 = 15 */
package poo;

import java.util.Scanner;

public class Ex08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n, soma = 0;
        
        do {
            System.out.print("Entre com um N termo: ");
            n = input.nextInt();
        } while (n <= 0);
        
        /*
        for (int i = n; i >= 1; i--) {
            soma = soma + i;
        }*/
        
        System.out.println("A soma dos n termos é: " + somaNTermos(n));
    }
    
    public static int somaNTermos(int termo){
        if(termo == 1){
            return 1;
        }
        
        return termo + somaNTermos(termo - 1);
    }
    
}
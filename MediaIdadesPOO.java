﻿/* 
PRIMEIRA QUESTÃO DE POO (AULA 14/03/2017)
1- Preencher doi vetores com tamanho 50, um com idades de 50 homens e outro com idades de 50 mulheres
2 - Calcular a média das idades dos homens e das mulheres
3 - Imprimir a quantidade de homens com idade superior a média das idades das mulheres e vice versa.
*/
package poo;

import java.util.Scanner;

public class PrimeiraQMetodo {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int f[] = new int[5];
        int m[] = new int[5];
        int somaF = 0, somaM = 0, qtdM =0, qtdF = 0;
        double mediaF, mediaM;
        
        //PREENCHE VETOR COM IDADES FEMININAS
        for (int i = 0; i < f.length; i++) {
            System.out.print("Entre com uma idade FEMININA na posição " + (i+1) + ": ");
            f[i] = input.nextInt();
            
            somaF = somaF + f[i];
        }
        mediaF = somaF / 5;
        
        System.out.println("------------------------------------------");
        
        //PREENCHE VETOR COM IDADES MASCULINAS
        for (int i = 0; i < m.length; i++) {
            System.out.print("Entre com uma idade MASCULINA na posição " + (i+1) + ": ");
            m[i] = input.nextInt();
            
            somaM = somaM + m[i];
        }
        mediaM = somaM / 5;
        
        //QUANTIDADE DE HOMENS COM IDADE SUPERIOR A MÉDIA DE IDADES FEMININA
        for (int i = 0; i < m.length; i++) {
            if(m[i] > mediaF){
                qtdM = qtdM + 1;
            }
        }
        
        //QUANTIDADE DE MULHERES COM IDADE SUPERIOR A MÉDIA DE IDADES MASCULINA
        for (int i = 0; i < m.length; i++) {
            if(f[i] > mediaM){
                qtdF = qtdF + 1;
            }
        }
        
        System.out.println("=====RESULTADO=====");
        System.out.print("Mulheres com idade superior à média da idade dos homens: " + qtdF);
        System.out.println("================================================");
        System.out.print("Homens com idade superior à média da idade das mulheres: " + qtdM);
        
    }
}
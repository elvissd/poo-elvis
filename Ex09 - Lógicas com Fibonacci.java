/*Usando a série Fibonacci faça um programa, em Java,
para informar quantas somas foram necessárias para que 
o resultado da série superasse um dado valor (N). 
Ex. N = 3;  R = 5
    N = 55; R = 11 */
package poo;

import java.util.Scanner;

public class Ex09Fibonacci {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Entre com um número N: ");
        int n = input.nextInt();
        
        String sequencia = "Sequência de fibonacci: {0,";
        //DAQUI PRA BAIXO CODIGO DO METODO FIBONACCI (SEM USAR NENHUM MÉTODO)
        long num1 = 0, num2 = 1, prox = 0, qtdSoma = 1;
        //Inicializei qtdSoma com 1 porque a sequencia já começa com dois termos (0 e 1) e já conta com
        //a soma dos dois.
        for (int i = 0; i <= n; i++){
            prox  = num1 + num2; //Neste caso prox é a segunda soma da sequência.
            num1 = num2;
            num2 = prox;
            
            if(num1 <= n){
                qtdSoma++;
            }
            
            sequencia = sequencia + num1 + " ";
        }
        sequencia = sequencia + "} e foram realizadas " + qtdSoma + " somas para superar o valor de " + n;
        System.out.println(sequencia);
    }
    
    public static String fibonacci(int n){
        String sequencia = "Sequência de fibonacci: ";
        
        long num1 = 0, num2 = 1, prox = 0, qtdSoma = 1;
        //Inicializei qtdSoma com 1 porque a sequencia já começa com dois termos (0 e 1) e já conta com
        //a soma dos dois.
        for (int i = 0; i <= n; i++){
            prox  = num1 + num2; //Neste caso prox é a segunda soma da sequência.
            num1 = num2;
            num2 = prox;
            
            if(num1 <= n){
                qtdSoma++;
            }
            sequencia = sequencia + num1 + " ";
        }
        sequencia = sequencia + " foram realizadas " + qtdSoma + " somas para superar o valor de " + n;
        return sequencia;
    }
    
    
    //Código diferente do método anterior (outro tipo de questão)
    //Para achar o enésimo termo da sequencia de fibonacci com recursividade:
    public static int fibonacciRecursivo(int n){
        if(n == 1 || n == 2){
            return 1;
        }
        
        return fibonacciRecursivo(n - 1) + fibonacciRecursivo(n - 2);
    }
    
}
﻿package poo;

import java.util.Random;
import java.util.Scanner;

public class MetodosGerais {
   
    public static void main(String[] args) {
        
    }
    
    //Equação = 1 + 1/1! + 1/2! + 1/3! ... até n termo
    public static void equacaoFatorial(int n) {
  	float e, fat;
        
        e=1;
        for (int i = 1; i <= n; i++){
            fat = 1;
            for (int j = 1; j <= i; j++)
		fat = fat * j;
		e = e + 1/fat;
		}
                
	System.out.println("Valor de E = " + e);	
    }
    
    //Fatorial normal
    public static int fatorial(int num){
        //Caso o número seja 0 já retorna 1, pois 0! = 1
        if(num == 0){
            return 1;
        }
        
        int fat = 1; //Defini fat inicialmente igual a 1, pois assim não preciso ir até o 1.
        for (int i = num; i > 1; i--) {
            fat *= i;
        }
        return fat;
    }
    
    //Fatorial Recursivo
    public static int fatorialRecursivo(int num){
        if(num == 0){
            return 1;
        }
        return num * fatorialRecursivo(num-1);
    }
    
    //Método para preencher um vetor normalmente
    public static void preencherVetor(int vet[]){
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < vet.length; i++) {
            System.out.print("Informe um número na posição " + (i+1) + ": ");
            vet[i] = input.nextInt();
        }
     }
    
    //Média de um vetor (ex: média das idades)
    public static double media(int vetor[], int qtd){
        int soma = 0;
        for (int i = 0; i < vetor.length; i++) {
           soma = soma + vetor[i];
        }
        double media = soma / vetor.length;
        return media;
    }
    
    //Imprimir vetor
    public static void imprimirVetor(int vetor[]){
        for (int i = 0; i < vetor.length; i++) {
            System.out.print(vetor[i] + " ");
        }
    }
    
    //Módulo de um número
    public static int modulo(int modulo){
        if(modulo < 0)
            modulo = modulo * -1;
        return modulo;
    }
    
    //Maior número
    public static void maiorNum(double num1, double num2){
        if(num1 > num2){
            System.out.print("O maior número é: " + num1);
        }else if(num1 < num2){
            System.out.print("O maior número é: " + num2);
        }else{
            System.out.println("Os números são iguais.");
        }
    }
    
    public static int arredondar(double num){
        int numFinal = (int) num;
        return numFinal;
    }
    
    //Soma de todos os númeoros de um vetor
    public static double somaVetor(double vetor[]){
        double soma = 0;
        for (int i = 0; i < vetor.length; i++) {
            soma = soma + vetor[i];
        }
        return soma;
    }
    
    //Preencher vetor Aleatório
    //Na classe main pedi para o usuário informar uma qtd que seria o índice do vetor
    public static void vetAleatorio(int qtdVet, boolean valorLogico){
        Random aleatorio = new Random();
        int vetor[] = new int[qtdVet];
        
        //Preenchi o vetor com números aleatórios
        for (int i = 0; i < vetor.length; i++) {
            //Exemplo pra preencher apenas com número pares ou ímpares
            //dependendo do valor lógico (true ou false).
            if(valorLogico){
                vetor[i] = aleatorio.nextInt(100)*2;
            }
            vetor[i] = aleatorio.nextInt(10+1);
        }
        
    }
    
    //Ordenar vetor - Bubble sort (Com algum erro)
    public static void ordenarVetor(int vetor[]){
        int aux = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(vetor[i] < vetor[j]){
                    aux = vetor[i];
                    vetor[i] = vetor[j];
                    vetor[j] = aux;
                }
            }
        }
    }

    //Ver quantas unidades tem um número Ex: 452; r: 3
    public static int qtdUnidadesNumero(int num){
        String aux = Integer.toString(num);
        //aux.length(); Para ver o tamanho das unidades
        return aux.length();
    }
    
}
package poo;

import java.util.Scanner;

public class Ex10PorExtenso {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numero;
        String extenso = "";
        
        System.out.print("Entre com um número: ");
        numero = input.nextInt();
        
        while(numero < 0 || numero > 100000){
            System.out.print("Número inválido! Digite novamente no intervalo de 0 a 100 mil: ");
            numero = input.nextInt();
        }
        
        
        String segundaCasaDezena[] = {"","dez","vinte","trinta","quarenta",
            "cinquenta","sessenta","setenta", "oitenta","noventa"};
        
        String terceiraCasaUnidade[] = {"", " e um ", " e dois ", " e três ", " e quatro ",
            " e cinco ", " e seis ", " e sete ", " e oito ", " e nove "};
        
        String quartaCasaCentena[] = {""," Cento", " Duzentos", " Trezentos", " Quatrocentos",
            " Quinhentos", " Seiscentos", " Setecentos", " Oitocentos", " Novecentos"};
        
        String dezena[] = {"","dez"," vinte "," trinta "," quarenta "," cinquenta ", " sessenta ",
            " setenta "," oitenta "," noventa "};
        
        String unidade[] = {"Zero", "Um ", "Dois ", "Três ", "Quatro ",
            "Cinco ", "Seis ", "Sete ", "Oito ", "Nove "};
        
        //SEGUNDA CASA DECIMAL OU CEM MIL (DA ESQUERDA PARA A DIREITA) 123.000
        if(numero == 100000){
            extenso = "Cem mil";
        }else if(numero/100000 == 0 && numero/1000 != 0){
            if( (numero/1000)%10 == 0){
                extenso = segundaCasaDezena[numero/10000] + " mil ";
            }else{
                extenso = segundaCasaDezena[numero/10000] + 
                        terceiraCasaUnidade[(numero/1000)%10] + " mil ";
            }
        }
        
        //QUARTA CASA DECIMA DA ESQUERDA PRA DIREITA 000.123
        if( (numero%1000) == 100){
            extenso = extenso + " e cem";
        }else if( (numero%1000) != 0 ){
            extenso = extenso + quartaCasaCentena[(numero%1000)/100];
        }
        if( (numero%100)/10 != 0 && numero%10 == 0 ){
            extenso = extenso + dezena[(numero%100)/10] + " e" + unidade[(numero%10)];
        }
        
        if( (numero/10) == 0 && (numero/100) == 0 ){
            extenso = unidade[numero];
        }
        
        System.out.println(extenso); 
    }
}
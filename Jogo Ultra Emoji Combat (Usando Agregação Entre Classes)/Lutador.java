package UltraEmojiCombat;

public class Lutador {
    private String nome, nacionalidade, categoria;
    private float altura, peso;
    private int idade, vitorias, derrotas, empates;
    
    
    public Lutador(String no, String na, int id, float alt, float pe, int vit,
            int der, int emp){
        this.nome = no;
        this.nacionalidade = na;
        this.altura = alt;
        this.setPeso(pe); // Aqui foi obrigatório usar o set, nos outros casos usa o que quiser.
        this.idade = id;
        this.vitorias = vit;
        this.derrotas = der;
        this.empates = emp;
        
    }
    
    public void apresentar(){
        System.out.println("--------------------------");
        System.out.println("APRESENTANDO O LUTADOR!");
        System.out.println("Lutador: " + this.getNome());
        System.out.println("Nacionalidade: " + this.getNacionalidade());
        System.out.println(this.getIdade() + " anos.");
        System.out.println(this.getAltura() + " m de altura.");
        System.out.println("Pesando " + this.getPeso() + "Kg");
        System.out.println("Ganhou: " + this.getVitorias());
        System.out.println("Perdeu: " + this.getDerrotas());
        System.out.println("Empatou: " + this.getEmpates());
    }
    
    public void status(){
        System.out.println(this.getNome());
        System.out.println("É um peso " + this.getCategoria());
        System.out.println(this.getVitorias() + " vitórias");
        System.out.println(this.getDerrotas() + " derrotas");
        System.out.println(this.getEmpates() + " empates");
    }
    
    public void ganharLuta(){
        this.setVitorias(this.getVitorias() + 1);
    }
    
    public void perderLuta(){
        this.setDerrotas(this.getDerrotas() + 1);
    }
    
    public void empatarLuta(){
        this.setEmpates(this.getEmpates() + 1);
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nom) {
        this.nome = nom;
    }

    public String getNacionalidade() {
        return this.nacionalidade;
    }

    public void setNacionalidade(String nacional) {
        this.nacionalidade = nacional;
    }

    public double getAltura() {
        return this.altura;
    }

    public void setAltura(float altu) {
        this.altura = altu;
    }

    public double getPeso() {
        return this.peso;
    }

    public void setPeso(float pes) {
        this.peso = pes;
        setCategoria();
    }
    
    private void setCategoria(){
        if(this.getPeso() < 52.2){
            this.categoria = "Inválido";
        }else if(this.getPeso() <= 70.3){
            this.categoria = "Leve";
        }else if(this.getPeso() <= 83.9){
            this.categoria = "Medio";
        }else if(this.getPeso() <= 120.2){
            this.categoria = "Pesado";
        }else{
            this.categoria = "Inválido";
        }
    }
    
    public String getCategoria(){
        return this.categoria;
    }

    public int getIdade() {
        return this.idade;
    }

    public void setIdade(int ida) {
        this.idade = ida;
    }

    public int getVitorias() {
        return this.vitorias;
    }

    public void setVitorias(int vitori) {
        this.vitorias = vitori;
    }

    public int getDerrotas() {
        return this.derrotas;
    }

    public void setDerrotas(int derrot) {
        this.derrotas = derrot;
    }

    public int getEmpates() {
        return this.empates;
    }

    public void setEmpates(int emp) {
        this.empates = emp;
    }
    
    
    
}
package UltraEmojiCombat;

import java.util.Random;

public class Luta {
    private Lutador desafiado; // Intância de lutador vai ser o desafiado. Tipo abstrato de dados
    private Lutador desafiante;
    private int rounds;
    private boolean aprovada;
    
    public void marcarLuta(Lutador l1, Lutador l2){
        if(l1.getCategoria().equals(l2.getCategoria()) && (l1 != l2)){
            this.aprovada = true;
            this.desafiado = l1;
            this.desafiante = l2;
        }else{
            this.aprovada = false;
        }
    }
    
    public void lutar(){
        if(this.aprovada){
            System.out.println("### DESAFIADO ###");
            desafiado.apresentar();
            System.out.println("### DESAFIANTE ###");
            desafiante.apresentar();
            
            //Gera um vencedor aleatório
            Random aleatorio = new Random();
            int vencedor = aleatorio.nextInt(3);
            
            System.out.println("==== RESULTADO DA LUTA ====");
            switch(vencedor){
                case 0: //Empate
                    System.out.println("Empatou!");
                    this.desafiado.empatarLuta();
                    this.desafiante.empatarLuta();
                    break;
                case 1: //Desafiado vence
                    System.out.println(this.desafiado.getNome() + " venceu a luta!");
                    this.desafiado.ganharLuta();
                    this.desafiante.perderLuta();
                case 2: //Desafiante vence
                    System.out.println(this.desafiante.getNome() + " venceu a luta!");
                    this.desafiante.ganharLuta();
                    this.desafiado.perderLuta();
                    break;
            }
            System.out.println("==============================");
        }else{
            System.out.println("Luta não pode acontecer.");
        }
    }
    
    public void setDesafiado(Lutador dd){
        this.desafiado = dd;
    }
    
    public Lutador getDesafiado(){
        return this.desafiado;
    }
    
}
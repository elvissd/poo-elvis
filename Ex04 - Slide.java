/* Faça uma função que retorne o módulo de um determinado valor;
Faça uma função que recebe dois números reais e retorna o maior entre eles;
Faça uma função que arredonda um número decimal; */
package poo;

import java.util.Scanner;

public class Ex04 {
    public static void main(String[] args) {
        Scanner input  = new Scanner(System.in);
        
        System.out.print("Informe um número que se deseja saber o módulo: ");
        int num = input.nextInt();
        System.out.println("O módulo de " + num + " é |" + modulo(num) + "|");
        
        System.out.println("Entre com dois números REAIS: ");
        double real1 = input.nextDouble();
        double real2 = input.nextDouble();
        maiorNum(real1, real2);
        
        System.out.print("Entre com um número decimal para arredondar: ");
        float numDecimal = input.nextFloat();
        System.out.print("O número " + numDecimal + " arredondado fica: " + arredondar(numDecimal));
        
    }
    
    public static int modulo(int modulo){
        if(modulo < 0)
            modulo = modulo * -1;
        return modulo;
    }
    
    public static void maiorNum(double num1, double num2){
        if(num1 > num2){
            System.out.print("O maior número é: " + num1);
        }else if(num1 < num2){
            System.out.print("O maior número é: " + num2);
        }else{
            System.out.println("Os números são iguais.");
        }
        
    }
    
    public static int arredondar(float num){
        int numFinal = (int) num;
        return numFinal;
    }
    
}
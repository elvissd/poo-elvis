/* Faça um programa que possui uma função que identifica se a soma dos valores 
de um vetor corresponde a um resultado X. A função deverá receber como parâmetro:
-um vetor “V”, contendo a lista de valores decimais;
-um valor inteiro “N”, representando a quantidade de valores que o vetor possui
-um valor decimal “X”, que representa o total da soma.
A função deverá retornar verdadeiro caso o valor X seja igual ao valor da soma dos elementos do vetor.
*/
package poo;

import java.util.Scanner;


public class Ex03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Informe uma quantidade de números para o vetor: ");
        int qtd = input.nextInt();
        
        System.out.print("Informe um resultado X para comparar com a soma do vetor: ");
        int resultadoX = input.nextInt();
        
        double numeros[] = new double[qtd];
        preencheVetor(numeros);
        double totalSoma = somaVetor(numeros);
        
        boolean resposta = identificaSoma(resultadoX, totalSoma);
        
        System.out.println("RESULTADO: " + resposta);
        
    }
    
    public static double somaVetor(double vetor[]){
        double soma = 0;
        
        for (int i = 0; i < vetor.length; i++) {
            soma = soma + vetor[i];
        }
        
        return soma;
    }
    
    
    static void preencheVetor(double vetor[]){
        Scanner input = new Scanner(System.in);
        
        for (int i = 0; i < vetor.length; i++) {
            System.out.print("Informe um número na posição " + (i+1) + ": ");
            vetor[i] = input.nextInt();
        }
        
    }
    
    public static boolean identificaSoma(double resultado, double valorSoma){
        boolean valorLogico;
        
        if(valorSoma == resultado){
            valorLogico = true;
        }else
            valorLogico = false;
        
        return valorLogico;
    }
    
}
/*Ver se um n�mero � �mpar ou par usando fun��es*/
package Funcoes;

import java.util.Scanner;

public class ParImpar {
    
    //Classe principal
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        int numero;
        
        System.out.print("Entre com um n�mero inteiro: ");
        numero = input.nextInt();
        
        paridade(numero);
        
    }
    
    //Fun��o (M�todo) onde est� a f�rmula para calcular paridade
    public static void paridade(int numero) {
        if(numero % 2 == 0){
            System.out.println("O n�mero " + numero + " � PAR.");
        }else{
            System.out.println("O n�mero " + numero + " � �MPAR");
        } 
    }
    
}
package treinamentoobi;

import java.util.Scanner;

public class TreinamentoOBI {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double pesoIdeal = 0;
        
        System.out.print("Entre com a sua altura: ");
        double altura = input.nextDouble();
        System.out.print("Informe o seu sexo (masculino ou feminino): ");
        String sexo = input.next();
        
        if(sexo.equalsIgnoreCase("masculino")){
            pesoIdeal = pesoIdealHomens(altura);
            System.out.printf("Seu peso ideal é: %.2f", pesoIdeal);
        }else if(sexo.equalsIgnoreCase("feminino")){
            pesoIdeal = pesoIdealMulheres(altura);
            System.out.printf("Seu peso ideal é: %.2f", pesoIdeal);
        }
        
        System.out.println("Informe o seu peso: ");
        double peso = input.nextInt();
        
        if(peso < pesoIdeal){
            System.out.println("Você está abaixo do peso ideal!");
        }else if(peso > pesoIdeal){
            System.out.println("Você está acima do peso ideal!");
        }else{
            System.out.println("Você está dentro do peso ideal.");
        }
        
        
    }
    
    public static double pesoIdealHomens(double h){
        double pesoIdeal = (72.7*h) - 58;
        return pesoIdeal;
    }
    
    public static double pesoIdealMulheres(double h){
        double pesoIdeal = (62.1*h) - 44.7;
        return pesoIdeal;
    }
    
}
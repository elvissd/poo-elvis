import java.util.*;

public class Q2Divisores {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n, qtdDivisores = 0;
        
        do{
            n = input.nextInt();
        }while (n < 1 || n > 10000);
        
        for (int i = 1; i <= n; i++) {
            if(n%i == 0){
               qtdDivisores++; 
            }
        }
        
        System.out.println(qtdDivisores);
    }
}
package aula06;

public class ControleRemoto implements Controlador{
    //Atributos
    private int volume;
    private boolean ligado;
    private boolean tocando;
    //Métodos Especiais
    public ControleRemoto() {
        this.volume = 50;
        this.ligado = false;
        this.tocando = false;
    }
    
    private int getVolume(){
        return this.volume;
    }
    private void setVolume(int v){
        this.volume = v;
    }
    private boolean getLigado(){
        return this.ligado;
    }
    private void setLigado(boolean l){
        this.ligado = l;
    }
    private boolean getTocando(){
        return this.tocando;
    }
    private void setTocando(boolean t){
        this.tocando = t;
    }
    
    //Métodos Abstratos
    @Override
    public void ligar(){
        this.setLigado(true);
    }

    @Override
    public void desligar(){
        this.setLigado(false);
    }

    @Override
    public void abrirMenu(){
        System.out.println("-----MENU-----");
        System.out.println("Ligado: " + this.getLigado());
        System.out.println("Tocando?: " + this.getTocando());
        System.out.print("VOLUME: " + this.getVolume() + "% ");
        for(int i = 0; i < this.getVolume(); i += 10){
            System.out.print("|");
        }
    }

    @Override
    public void fecharMenu(){
        System.out.println("Fechando menu...");
    }

    @Override
    public void maisVolume(){
        if(this.getLigado()){
            this.setVolume(this.getVolume() + 1);
        }else{
            System.out.println("TV desligada! Impossível aumentar volume.");
        }
    }

    @Override
    public void menosVolume(){
        if(this.getLigado()){
            this.setVolume(this.getVolume() - 1);
        }else{
            System.out.println("TV desligada! Impossível diminuir volume.");
        }
    }

    @Override
    public void ligarMudo(){
        if(this.getLigado() && this.getVolume() > 0){
            this.setVolume(0);
        }else{
            System.out.println("Ligar mudo falhou.");
        }
    }

    @Override
    public void desligarMudo(){
        if(this.getLigado() && this.getVolume() == 0){
            this.setVolume(50);
        }
    }

    @Override
    public void play(){
        if(this.getLigado() &&  !(this.getTocando())){
            this.setTocando(true);
        }else{
            System.out.println("Reproduzir falhou.");
        }
    }

    @Override
    public void pause(){
        if(this.getLigado() && this.getTocando()){
            this.setTocando(false);
        }else{
            System.out.println("Pausar falhou.");
        }
    }
    
    
}
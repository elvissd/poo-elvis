package herancaparte2;

public class Bolsista extends Aluno{
    private float bolsa;
    
    public void renovarBolsa(){
        System.out.println("Renovando bolsa de " + this.nome);
    }

    public float getBolsa() {
        return bolsa;
    }

    public void setBolsa(float bolsa) {
        this.bolsa = bolsa;
    }
    
    
    
    //Sobrepos o método abaixo que existia em Aluno com o Override
    //Esse pagarMensalidade() não é a mesma coisa que o pagarMensalidade() de Aluno
    @Override
    public void pagarMensalidade(){
        System.out.println(this.nome + " é bolsista, pagamento facilitado.");
    }
    
}
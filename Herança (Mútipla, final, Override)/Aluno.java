package herancaparte2;

public class Aluno extends Pessoa{
//public final class Aluno extends Pessoa{
    //Se eu usar o final em Class a classe não poderá gerar "filhos"
    //Uso final quando não quero que a classe gere filhos
    private int matricula;
    private String curso;
    
    //public final void pagarMensalidade(){
    //A mesma coisa para os métodos, a palavra reservada final impede que o método seja sobrescrito
    //Uso final quando não quero que o método seja sobreposto
    public void pagarMensalidade(){
        System.out.println("Pagando mensalidade do aluno " + this.nome);
        //Pode fazer this.nome direto sem o get porque o atributo agora é protegido em Pessoa
        //O atributo protegido dá acesso à classe e conseqeuntemente às subclasses
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    
    
}